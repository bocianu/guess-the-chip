Jaka to gra gra? (guess the chiptune)
=====================================

Raspberry Pi software to run contest "Guess the Chip",
where contestants try to guess game name,
related to listened chiptune.

Written in python, uses hardware buttons, tk graphical gui,
and XMMS2 as an SAP player.

Writen for party **WAPNIAK 2016**

Requirements:
-------------

packages you need:

 * python-tk
 * python-imaging-tk
 * python-xmmsclient
 * xmms2
 * xmms2-plugin-gme

Atari font: http://members.bitstream.net/marksim/atarimac/acttfpc.zip

Songs stolen from **ASMA** -
Atari SAP Music Archive -
http://asma.atari.org/ ;)

