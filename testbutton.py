#!/usr/bin/python

import RPi.GPIO as GPIO
from config import *

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_PLAYER1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(PIN_PLAYER2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(PIN_PLAYER3, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)


while True:
    print "B1: %s   B2: %s   B3: %s   " % (GPIO.input(PIN_PLAYER1), GPIO.input(PIN_PLAYER2), GPIO.input(PIN_PLAYER3))
