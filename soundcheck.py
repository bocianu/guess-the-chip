import xmmsclient
import os
import sys
import time

xmms = xmmsclient.XMMS("tutorial1")

try:
    xmms.connect(os.getenv("XMMS_PATH"))
except IOError, detail:
    print "Connection failed:", detail
    sys.exit(1)

xmms.playlist_clear()
result = xmms.playlist_add_url("file://./test/Turbo_Snail.sap")

result = xmms.playback_start()
result.wait()

if result.iserror():
    print "playback start returned error, %s" % result.get_error()
else:
    time.sleep(3.5)
    result = xmms.playback_stop()
    result.wait()
    sys.exit(0)



