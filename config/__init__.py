__author__ = 'wbocianski'

USERS_FILE_NAME = "./datastore/users.json"
SONGS_FILE_NAME = "./datastore/songs.json"

SONGS_LIBRARY = "./songs"

BGCOLOR = "#2d759c"
BGCOLOR_GUESS = "#003060"
BGCOLOR_RIGHT = "#006000"
BGCOLOR_WRONG = "#600000"
FGCOLOR = "#81c9f0"
FGCOLORBRIGHT = "white"

GUI_BASE_FONT_SIZE = 24
GUI_FONT = 'Atari Classic Chunky'

CONSOLE_PROMPT = '> '
EMPTY_PLAYER = '----------'

PIN_PLAYER1 = 2
PIN_PLAYER2 = 3
PIN_PLAYER3 = 4

############### DO NOT CHANGE #################

FALSE = 0
TRUE = 1

CLEAR_SCREEN = chr(27) + "[2J"

class txtcolor:
    PINK = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    DARKGREY = '\033[90m'
    LIGHTBLUE = '\033[96m'
    PROMPT = '\033[96m'
    STATUS = '\033[93m'
    DEFAULT = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class PlayerFrames:
    WHO = 0
    WHO_BLINK = 1

    READY = 2
    READY_BLINK = 3

    READY_L = 4
    READY_L_BLINK = 5

    DOWN = 6
    DOWN_BLINK = 7

    DOWN_L = 8
    DOWN_L_BLINK = 9

    UP = 10
    UP_BLINK = 11

    UP_L = 12
    UP_L_BLINK = 13


