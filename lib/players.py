import sys
import random
from config import *

player = {
    'name': None,
    'score': 0,
    'punished': 0,
    'alive': 1
}

class Players(object):

    def clearPlayers(self):
        self.players = [player.copy(), player.copy(), player.copy()]

    def randomizePlayers(self, rounds):
        print txtcolor.GREEN + "randomizing..."
        self.clearPlayers()
        unames = self.getAvailablePlayers(int(rounds))
        pnum = 0
        while len(unames) > 0 and pnum < 3:
            player = random.choice(unames)
            unames.remove(player)
            self.setPlayer(pnum, player)
            pnum += 1

    def setPlayer(self, pnum, name):
        self.players[pnum].update(self.users.data[name].copy())
        self.players[pnum]['name'] = name

    def playersAssignedNum(self):
        return sum(player['name'] is not None for player in self.players)

    def canGuess(self, pnum):
        return self.players[pnum]['alive'] == 1 and self.players[pnum]['punished'] == 0

    def rewardPlayer(self, pnum):
        self.players[pnum]['score'] += self.score

    def getLooser(self):
        retval = -1
        scores = []
        for pnum in 0, 1, 2:
            if self.players[pnum]['alive'] == 1:
                scores.append(self.players[pnum]['score'])
        minimal = min(scores)
        for pnum in 0, 1, 2:
            if self.players[pnum]['alive'] == 1:
                if self.players[pnum]['score'] == minimal:
                    if retval != -1:
                        return -1
                    else:
                        retval = pnum
        return retval

    def killPlayer(self, pnum):
        self.players[pnum]['alive'] = 0
        user = self.users.data[self.players[pnum]['name']]
        user['dead'] = TRUE
        self.users.save()

    def setWinner(self):
        for pnum in 0, 1, 2:
            if self.players[pnum]['alive'] == 1:
                print txtcolor.YELLOW + "%s won the game!" % (self.players[pnum]['name'])
                user = self.users.data[self.players[pnum]['name']]
                user['wins'] += 1
                self.users.save()
                return pnum

    def alivePlayersNum(self):
        still_alive = 0
        for pnum in 0, 1, 2:
            still_alive += self.players[pnum]['alive']
        return still_alive

    def playersRoundUp(self):
        for pnum in 0, 1, 2:
            user = self.users.data[self.players[pnum]['name']]
            user['score'] += self.players[pnum]['score']
            user['rounds'] += 1
        self.users.save()


    def promptPlayers(self):
        pnum = 0
        start = False
        while True:
            while True:
                self.showStatus()
                entry = self.prompt(
                    'Which player you wish to assign [1-3]?\n' + txtcolor.DEFAULT +
                    'Type "auto" to pick random users\n'
                    '"list" to get available users\n'
                    '"clear" to clear all assignments%s',
                    ('\n"start" to start the game' if self.playersAssignedNum()>1 else "")
                )
                if entry == 'quit' or entry == 'exit':
                    sys.exit(0)
                if entry == 'start':
                    start = True
                    break
                if entry in ['1', '2', '3']:
                    pnum = int(entry) - 1
                    break
                if entry == 'list':
                    self.listAvailablePlayers()
                elif entry == 'clear':
                    self.clearPlayers()
                elif entry == 'auto':
                    self.clearPlayers()
                    self.listAvailablePlayers()
                    rounds = self.prompt('With how many rounds already played?')
                    if rounds.isdigit():
                        self.randomizePlayers(rounds)
                else:
                    self.showError('bad choice... try again!')
            if not start:
                while True:
                    unames = self.getAvailablePlayers()
                    entry = self.prompt('Enter PLAYER%d name:\n' + txtcolor.DEFAULT +
                                        'Type "list" for players list\n'
                                        '"clear" to clear assignment',
                                        (int(pnum) + 1))
                    if entry == 'quit' or entry == 'exit':
                        sys.exit(0)
                    if entry == 'list':
                        self.listAvailablePlayers()
                    elif entry == 'clear':
                        self.players[pnum] = player.copy()
                        self.players[pnum]['name'] = None
                        break
                    elif entry in unames:
                        self.setPlayer(pnum, entry)
                        break
                    else:
                        self.showError('player unavailable!')
            # start
            else:
                if self.playersAssignedNum() > 1:
                    break
                else:
                    start = False
                    self.showError('You need more players to start the game!')

