#!/usr/bin/python

from lib.dao import DAO
from config import *


class UsersDAO(DAO):
    def show(self):
        print "USERS LIST:"
        super(UsersDAO, self).show()
        super(UsersDAO, self).count()

    def getAlive(self):
        return {k: v for k, v in self.data.items() if v['dead'] == FALSE}

    def getAliveWithRounds(self, rounds):
        return {k: v for k, v in self.data.items() if v['rounds'] == rounds and v['dead'] == FALSE}


template = {
    'score': 0,
    'dead': FALSE,
    'shots': 0,
    'rounds': 0,
    'wins': 0
}
