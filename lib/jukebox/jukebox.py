import xmmsclient, sys
from lib.songs import *


class Jukebox():
    def __init__(self, clearList=False):
        self.xmms = xmmsclient.XMMS("jukebox")
        self.current = None
        try:
            self.xmms.connect(os.getenv("XMMS_PATH"))
        except IOError, detail:
            print "Connection to xmms2d failed:", detail
            sys.exit(1)
        try:
            self.xmms.config_set_value("playlist.repeat_one", "1")
        except AttributeError:
            self.xmms.configval_set("playlist.repeat_one", "1")

        songs.load()
        if clearList:
            print "Reloading list"
            songs.clear()
            songs.reload()

    def clear(self):
        self.xmms.playlist_clear()

    def add(self, file):
        self.xmms.playlist_add_url("file://" + file)

    def play(self, song):
        if songs.data.has_key(song):
            self.current = song
            songs.setPlayed(song)
            if self.isPlaying():
                self.stop()
            self.clear()
            self.add(songs.data[song]['path'])
            result = self.xmms.playback_start()
            result.wait()
            if result.iserror():
                print "playback start returned error, %s" % result.get_error()
        else:
            print "unknown song: %s" % song

    def stop(self):
        result = self.xmms.playback_stop()
        result.wait()
        if result.iserror():
            print "playback stop returned error, %s" % result.get_error()

    def isPlaying(self):
        result = self.xmms.playback_status()
        result.wait()
        return result.value() == xmmsclient.PLAYBACK_STATUS_PLAY

    def playRandom(self):
        song = songs.getRandom()
        if song:
            self.play(song)
        else:
            print 'All songs played'

    def playNext(self):
        song = songs.getFirstNotPlayed()
        if song:
            self.play(song)
        else:
            print 'All songs played'
