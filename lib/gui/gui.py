from Tkinter import *
from PIL import Image, ImageTk
from config import *
from threading import Timer
import random
import os


class GUI(object):
    def __init__(self):
        self.root = Tk()
        self.screenwidth = self.root.winfo_screenwidth()
        self.screenheight = self.root.winfo_screenheight()
        self.root.attributes('-fullscreen', True)
        #        self.root.geometry("{0}x{1}+0+0".format(self.screenwidth, self.screenheight))
        self.pad3 = self.root.winfo_screenwidth() / 3 - 5

        self.playersStr = [
            {'name': StringVar(), 'score': StringVar(), 'image': PhotoImage()},
            {'name': StringVar(), 'score': StringVar(), 'image': PhotoImage()},
            {'name': StringVar(), 'score': StringVar(), 'image': PhotoImage()}
        ]
        self.players = [{}, {}, {}]

        self.playerFrames = []
        for file in sorted(os.listdir('./assets/')):
            oryginal = Image.open('./assets/' + file)
            resized = oryginal.resize((int(self.pad3 * 0.7), (self.screenheight / 2)), Image.ANTIALIAS)
            self.playerFrames.append(ImageTk.PhotoImage(resized))

        self.screenwidth = self.root.winfo_screenwidth()
        self.screenheight = self.root.winfo_screenheight()
        self.root.attributes('-fullscreen', True)
        #        self.root.geometry("{0}x{1}+0+0".format(self.screenwidth, self.screenheight))
        self.pad3 = self.root.winfo_screenwidth() / 3 - 5
        self.fontsize = GUI_BASE_FONT_SIZE

        self.root.bind("<Key>", self.key)
        self.root.bind("<Button-1>", self.mouseclick)

        self.labelStyle = {'bg': BGCOLOR, 'fg': FGCOLORBRIGHT, 'justify': CENTER, 'height': 3,
                           'font': (GUI_FONT, self.fontsize)}
        self.scoreStyle = {'bg': BGCOLOR, 'fg': FGCOLOR, 'justify': CENTER, 'height': 2,
                           'font': (GUI_FONT, int(self.fontsize * 2.5))}
        self.imgStyle = {'bg': BGCOLOR, 'width': self.pad3, 'height': self.screenheight / 2}
        self.packConf = {'anchor': S, 'side': BOTTOM, 'expand': 0, 'fill': X}

        self.toptext = StringVar()
        self.toptext.set('Jaka to GRA gra?')

        self.key = 0;

    def mainLayout(self):
        self.frame = Frame(self.root, bg=BGCOLOR)
        self.frame.pack(anchor=N, fill=BOTH, expand=True)

        self.topbar = Frame(self.frame, bg=BGCOLOR, width=self.screenwidth, height=self.screenheight / 5)
        self.topbar.pack(expand=False)

        self.topmsg = Label(self.topbar, bg=BGCOLOR, fg=FGCOLORBRIGHT, compound=CENTER, textvariable=self.toptext,
                            justify=CENTER, height=2,
                            font=(GUI_FONT, int(self.fontsize * 1.6)))
        self.topmsg.pack(anchor=N, side=TOP, expand=0, fill=X)

    def key(self, event):
        if event.char == '\x1b':
            self.root.destroy()
        if event.char == '1' or event.char == '2' or event.char == '3':
            self.key = event.char
        print "pressed", repr(event.char)

    def mouseclick(self, event):
        self.frame.focus_set()
        print "clicked at", event.x, event.y

    def setTopText(self, txt):
        self.toptext.set(txt)
        self.root.update()

    def setPlayerName(self, pnum, name):
        self.playersStr[pnum]['name'].set(str(name))
        self.root.update()

    def setPlayerScore(self, pnum, score):
        self.playersStr[pnum]['score'].set(str(score))
        self.root.update()

    def setPlayerFrame(self, pnum, frame, safe=True):
        if safe and self.players[pnum]['frameNum'] % 2 == 1:
            frame += 1
        self.players[pnum]['frameNum'] = frame
        self.players[pnum]['img'].config(image=self.playerFrames[frame])
        self.root.update()

    def showPlayer(self, pnum):
        self.players[pnum]['frame'].pack(anchor=W, fill=Y, expand=1, side=LEFT)
        self.players[pnum]['label'].pack(self.packConf)
        self.players[pnum]['score'].pack(self.packConf)
        self.players[pnum]['img'].pack(self.packConf)

    def highlightPlayer(self, pnum, color):
        self.players[pnum]['score'].config(bg=color)
        self.root.update()

    def highlightReser(self):
        for i in 0, 1, 2:
            self.players[pnum]['score'].config(bg=BGCOLOR)
        self.root.update()

    def initPlayers(self):

        for i in 0, 1, 2:
            self.setPlayerName(i, 'Player ' + str(i + 1))
            self.setPlayerScore(i, 0)

            self.players[i]['frame'] = Frame(self.frame, bg=BGCOLOR, width=self.pad3)
            self.labelStyle['textvariable'] = self.playersStr[i]['name']
            self.scoreStyle['textvariable'] = self.playersStr[i]['score']
            self.imgStyle['image'] = self.playersStr[i]['image']
            self.players[i]['label'] = Label(self.players[i]['frame'], self.labelStyle)
            self.players[i]['score'] = Label(self.players[i]['frame'], self.scoreStyle)
            self.players[i]['img'] = Label(self.players[i]['frame'], self.imgStyle)

            self.setPlayerFrame(i, PlayerFrames.WHO, False)
            self.showPlayer(i)

    def blink(self, pnum):
        self.setPlayerFrame(pnum, self.players[pnum]['frameNum'] + 1, False)
        self.root.after(100, self.unblink, pnum)

    def unblink(self, pnum):
        self.setPlayerFrame(pnum, self.players[pnum]['frameNum'] - 1, False)
        self.root.after(random.randint(1000, 8000), self.blink, pnum)

    def blinkStart(self):
        for pnum in 0, 1, 2:
            self.root.after(random.randint(1000, 8000), self.blink, pnum)

    def blinkStop(self):
        for pnum in 0, 1, 2:
            pass
