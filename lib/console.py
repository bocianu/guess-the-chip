from config import *

class GameConsole(object):
    def showPlayers(self):
        print txtcolor.GREEN + "\nPLAYER1 : %-12s     PLAYER2 : %-12s     PLAYER3 : %-12s" \
                              % (self.players[0]['name'] or EMPTY_PLAYER, self.players[1]['name'] or EMPTY_PLAYER,
                                 self.players[2]['name'] or EMPTY_PLAYER)
        print txtcolor.DEFAULT + "SCORE   : %-12s     SCORE   : %-12s     SCORE   : %-12s" \
                                 % (self.players[0]['score'], self.players[1]['score'], self.players[2]['score'])

    def showGameInfo(self):
        print txtcolor.STATUS + "\nGAME ROUND : %s    songs left: %s     players alive: %s" % (
            self.round, len(self.songs.getUnplayed(0)), len(self.users.getAlive()))

    def showStatus(self):
        self.showGameInfo()
        self.showPlayers()

    def getAvailablePlayers(self, roundsPlayed=0):
        unames = self.users.getAliveWithRounds(roundsPlayed).keys()
        for player in self.players:
            if player['name'] is not None and player['name'] in unames:
                unames.remove(player['name'])
        return unames

    def listAvailablePlayers(self):
        rmin = min([player['rounds'] for name, player in self.users.data.items()])
        rmax = max([player['rounds'] for name, player in self.users.data.items()])
        print txtcolor.GREEN + "\nAvailable users:"
        for rounds in range(rmin, rmax + 1):
            unames = self.getAvailablePlayers(roundsPlayed=rounds)
            unames.sort()
            if len(unames) != 0:
                print "%s%d rounds played (%d):\n  %s%s" % (txtcolor.DEFAULT, rounds, len(unames), txtcolor.BOLD, ', '.join(unames))

    def prompt(self, msg, params=()):
        print txtcolor.YELLOW + "\n" + msg % params
        return raw_input(txtcolor.PROMPT + CONSOLE_PROMPT)

    def showError(self, txt):
        print txtcolor.RED + "\nERROR: %s " % txt
        print txtcolor.DARKGREY + "press Enter to continue..." + txtcolor.DEFAULT
        raw_input()
