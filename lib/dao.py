import json
import os.path

class DAO(object):
    def __init__(self, resourceName, template, fileName):
        self.data = {}
        self.name = resourceName
        self.filename = fileName
        self.template = template

    def add(self, id):
        new = self.template.copy()
        self.data[id] = new
        self.save()

    def count(self):
        print "%s count : %d" % (self.name, len(self.data))

    def remove(self, id):
        if self.data.has_key(id):
            del self.data[id]
            self.save()

    def show(self):
        ids = self.data.keys()
        ids.sort()
        for id in ids:
            print "%s = %s" % (id, str(self.data[id]))

    def save(self):
        out_file = open(self.filename, "w")
        json.dump(self.data, out_file, indent=4)
        out_file.close()

    def load(self):
        self.data.clear()
        if os.path.isfile(self.filename):
            in_file = open(self.filename, "r")
            self.data.update(json.load(in_file))
            in_file.close()
        return self

    def clear(self):
        self.data.clear()
        self.save()

    def generate(self, max):
        self.data.clear()
        for i in range(1, max + 1):
            self.add(self.name + str(i))
        self.save()
