#!/usr/bin/python

from config import *
import os
from lib.dao import DAO
from config import *
import random


class SongsDAO(DAO):
    def add(self, name, path=''):
        self.template['path'] = path
        super(SongsDAO, self).add(name)

    def reload(self):
        self.clear()
        for file in os.listdir(SONGS_LIBRARY):
            name = os.path.splitext(file)[0].replace("_", " ")
            path = SONGS_LIBRARY + '/' + file
            self.add(name, path)

    def getFirstNotPlayed(self):
        ids = self.data.keys()
        ids.sort()
        for id in ids:
            if self.data[id]['played'] == 0:
                return id
        return None

    def setPlayed(self, id):
        self.data[id]['played'] += 1
        self.save()

    def getUnplayed(self, playedTreshold = 0):
        return {k: v for k, v in self.data.items() if v['played'] == playedTreshold}

    def getRandom(self, playedTreshold = 0):
        unplayed = self.getUnplayed(playedTreshold)
        count = len(unplayed)
        if count > 0:
            return random.choice(unplayed.keys())
        return None


template = {
    'path': '',
    'played': 0
}
