#!/usr/bin/python
import argparse
from lib.songs import songs

parser = argparse.ArgumentParser(description='Manipulate song list')
parser.add_argument('-l', action='store_true', help='lists all Songs')
parser.add_argument('-a', metavar='songname', nargs=1, help='adds Song')
parser.add_argument('-r', metavar='songname', nargs=1, help='removes Song')
parser.add_argument('-c', action='store_true', help='clear all Songs')
parser.add_argument('-g', metavar='number', nargs=1,  type=int, help='generate number of songs (clears all data)')
parser.add_argument('-n', action='store_true', help='reload all Songs')
parser.add_argument('-q', action='store_true', help='get random song')

args = parser.parse_args()
songs.load()

if args.a:
    songs.add(args.a[0])

if args.r:
    songs.remove(args.r[0])

if args.c:
    songs.clear()

if args.n:
    songs.reload()

if args.g:
    songs.generate(args.g[0])

if args.l:
    songs.show()
    songs.count()

if args.q:
    rnd = songs.getRandom()
    if rnd:
        songs.setPlayed(rnd)
        print (rnd)
    else:
        print "sorry mate, end of the list..."

if not any(vars(args).values()):
    parser.print_help()





