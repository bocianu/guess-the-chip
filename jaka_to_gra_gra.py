#!/usr/bin/python

import RPi.GPIO as GPIO
from lib.gui import GUI
from lib.users import users
from lib.songs import songs
from lib.jukebox import Jukebox
from config import *
from lib.console import GameConsole
from lib.players import Players
import time
import sys

jukebox = Jukebox()

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_PLAYER1, GPIO.IN)
GPIO.setup(PIN_PLAYER2, GPIO.IN)
GPIO.setup(PIN_PLAYER3, GPIO.IN, pull_up_down=GPIO.PUD_UP)

class Game(GameConsole, Players):
    def __init__(self):
        self.round = 1
        self.riddle = 1
        self.score = 0
        self.songs = songs.load()
        self.users = users.load()
        self.clearPlayers()
        self.timer = 0

        print CLEAR_SCREEN

    def assignPlayers(self):
        self.promptPlayers()
        for pnum in 0, 1, 2:
            gui.setPlayerName(pnum, game.players[pnum]['name'])
            gui.setPlayerFrame(pnum, PlayerFrames.READY)

    def setPlayersFrame(self, frame):
        for pnum in 0, 1, 2:
            gui.setPlayerFrame(pnum, frame)

    def highlightPlayers(self, color):
        for pnum in 0, 1, 2:
            gui.highlightPlayer(pnum, color)

    def unpunishPlayers(self):
        for pnum in 0, 1, 2:
            gui.setPlayerFrame(pnum, PlayerFrames.READY)
            gui.highlightPlayer(pnum, BGCOLOR)
            self.players[pnum]['punished'] = 0

    def punishPlayer(self, pnum):
        self.players[pnum]['punished'] = 1
        gui.setPlayerFrame(pnum, PlayerFrames.DOWN)
        gui.highlightPlayer(pnum, BGCOLOR_WRONG)


    def prepareSong(self):
        song = songs.getRandom()
        while True:
            self.showStatus()
            entry = self.prompt('Song is READY to play: %s\n' + txtcolor.DEFAULT +
                                'type "start" to play the song\ntype "another" to pick another', song)
            if entry == 'quit' or entry == 'exit':
                sys.exit(0)
            if entry == 'start' or entry == 's':
                return song
            if entry == 'another':
                song = songs.getRandom()
            else:
                print 'What??'

    def startSong(self, song):
        output = 0
        jukebox.play(song)
        time_now = time.time()
        gui.setTopText(str(self.score))
        gui.key = 0
        while self.timer > 0:

            if self.canGuess(0):
                if (not GPIO.input(PIN_PLAYER1)) or gui.key == '1':
                    output = 1
                    break
            if self.canGuess(1):
                if (not GPIO.input(PIN_PLAYER2)) or gui.key == '2':
                    output = 2
                    break
            if self.canGuess(2):
                if (not GPIO.input(PIN_PLAYER3)) or gui.key == '3':
                    output = 3
                    break

            ttime = time.time()
            if ttime - time_now >= 1:
                time_now = ttime
                self.score -= 10
                self.timer -= 1
                gui.setTopText(str(self.score))
            else:
                gui.root.update()

        jukebox.stop()
        return output

    def isRightAnswer(self, song):
        while True:
            entry = self.prompt('Is the answer correct (%s) ?\n' + txtcolor.DEFAULT + '"yes" or "no"', song)
            if entry.lower() == 'yes':
                return TRUE
            if entry.lower() == 'no':
                return FALSE
            self.showError("be precise: yes on no?")

gui = GUI()
gui.mainLayout()
gui.initPlayers()

game = Game()
# ----------------------------------------- MAIN LOOP



# -------- PLAYERS SELECTION
gui.blinkStart()
gui.setTopText('Losowanie Graczy')
game.assignPlayers()
game.round = 1
game.riddle = 1

while True:
    # -------- GAME START / ROUND START / RIDDLE START
    gui.setTopText('Zagadka ' + str(game.riddle) + ' - READY[]')
    game.unpunishPlayers()
    game.score = 200
    game.timer = 20

    # -------- PICK SONG
    print txtcolor.YELLOW + "Riddle: " + str(game.riddle)
    current_song = game.prepareSong()

    game.setPlayersFrame(PlayerFrames.READY)
    game.highlightPlayers(BGCOLOR)

    # -------- SONGPLAYS / START TIMER
    while True:
        result = game.startSong(current_song)

    # -------- SOMEONE GUESSES / TIME ENDED
        if result in (1, 2, 3):
            pnum = result - 1
            gui.highlightPlayer(pnum, BGCOLOR_GUESS)
            print txtcolor.PINK + '\nPlayer %s wants to guess!' % game.players[pnum]['name']

            # -------- GOOD ANSWER / ADD SCORE ^^^
            if game.isRightAnswer(current_song) == TRUE:
                gui.highlightPlayer(pnum, BGCOLOR_RIGHT)
                game.rewardPlayer(pnum)
                gui.setPlayerScore(pnum, game.players[pnum]['score'])
                gui.setPlayerFrame(pnum, PlayerFrames.UP)
                gui.setTopText("%s + %s" % (game.players[pnum]['name'], game.score))
                game.prompt(txtcolor.YELLOW + "%s scored %s points!" % (game.players[pnum]['name'], game.score))
                break

            # -------- BAD ANSWER / MUTE PLAYER ^^^
            else:
                game.unpunishPlayers()
                game.punishPlayer(pnum)
                game.prompt(txtcolor.PINK + '\nPlayer punished')



        # -------- NOBODY KNOWS - TIMEOUT ^^^
        elif result == 0:
            gui.setPlayerFrame(0, PlayerFrames.DOWN)
            gui.setPlayerFrame(1, PlayerFrames.DOWN)
            gui.setPlayerFrame(2, PlayerFrames.DOWN)
            gui.setTopText('KONIEC CZASU')
            game.prompt(txtcolor.PINK + '\nTime has ended...')
            break

    # -------- END RIDDLE
    game.riddle += 1

    # -------- ROUND NOT FINISHED (1 round = 6 riddles / 2 round = 4 riddles) ^^^
    if (game.round == 1 and game.riddle > 6) or (game.round == 2 and game.riddle > 4):

        looser = game.getLooser()
        if looser == -1:
            # add one more riddle when no looser
            pass
        else:
            game.killPlayer(looser)

            if (game.alivePlayersNum() == 1):
                # ------ END GAME
                winner = game.players[game.setWinner()]['name']
                gui.setTopText('zwyciezca: %s' % winner)
                game.prompt(txtcolor.PINK + '\nWinner: %s' % winner)
                break
            else:
            # -------- END ROUND
                game.round += 1
                game.riddle = 1

    # -------- UPDATE RESULTS

game.playersRoundUp()
game.users.save()
