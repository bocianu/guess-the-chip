#!/usr/bin/python
import argparse
from lib.users import users

parser = argparse.ArgumentParser(description='Manipulate users')
parser.add_argument('-l', action='store_true', help='lists all Users')
parser.add_argument('-a', metavar='username', nargs=1, help='adds User')
parser.add_argument('-r', metavar='username', nargs=1, help='removes User')
parser.add_argument('-c', action='store_true', help='clear all Users')
parser.add_argument('-g', metavar='number', nargs=1, type=int, help='generate number of users (clears all data)')

args = parser.parse_args()
users.load()

if args.a:
    users.add(args.a[0])

if args.r:
    users.remove(args.r[0])

if args.c:
    users.clear()

if args.g:
    users.generate(args.g[0])

if args.l:
    users.show()

if not any(vars(args).values()):
    parser.print_help()
