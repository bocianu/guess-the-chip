#!/usr/bin/python
import argparse
from lib.jukebox import Jukebox

parser = argparse.ArgumentParser(description='SAP Jukebox - play your SAPs!')
parser.add_argument('-c', action='store_true', help='reload List')
parser.add_argument('-n', action='store_true', help='play next song')
parser.add_argument('-r', action='store_true', help='play random song')
parser.add_argument('-p', metavar='songname', nargs=1, help='play selected song')
parser.add_argument('-s', action='store_true', help='stop playback')
args = parser.parse_args()

jukebox = Jukebox(args.c)

if args.r:
    jukebox.playRandom()
    print "playing: %s" % jukebox.current

if args.n:
    jukebox.playNext()
    print "playing: %s" % jukebox.current

if args.p:
    jukebox.play(args.p[0])
    print "playing: %s" % jukebox.current

if args.s:
    jukebox.stop()
    print "playback stopped"

if not any(vars(args).values()):
    print "SAP JukeBox"
    print "N - next Tune      R - random Tune"
    print "Press Q to quit"
    while True:
        entry = raw_input("Your choice: ").lower()
        if entry == "q":
            if jukebox.isPlaying():
                jukebox.stop()
            break
        if entry == "n":
            jukebox.playNext()
            print "playing: %s" % jukebox.current
        if entry == "r":
            jukebox.playRandom()
            print "playing: %s" % jukebox.current


